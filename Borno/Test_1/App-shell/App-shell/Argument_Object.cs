﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_shell
{
    interface IArgument_Object
    {
        string[] directories { get; }
        string[] files { get; }
        string firstFile { get; }
        string[] config_files { get; }
        string[] output_playlists { get;  }
        string[] input_playlists { get; }
        //string[] input_playlist_directories { get; }

    }

    class Argument_Object : IArgument_Object
    {
        private string[] _directories = null;
        private string[] _files = null;
        private string _firstFile = null;
        private string[] _config_files = null;
        private string[] _output_playlists;
        private string[] _input_playlists;
        //private string[] _input_playlist_directories;

        public string[] directories
        {
            get
            {
                return _directories;
            }

            private set
            {
                directories = _directories;
            }
        }
        public string[] files
        {
            get
            {
                return _files;
            }
            private set
            {
                files = _files;
            }
        }
        public string firstFile
        {
            get
            {
                return _firstFile;
            }

            private set
            {
                firstFile = _firstFile;
            }
        }
        public string[] config_files
        {
            get
            {
                return _config_files;
            }
            private set
            {
                config_files = _config_files;
            }
        }
        public string[] output_playlists
        {
            get
            {
                return _output_playlists;
            }
        }
        public string[] input_playlists
        {
            get
            {
                return _input_playlists;
            }
        }
        /*public string[] input_playlist_directories
        {
            get
            {
                return _input_playlist_directories;
            }
        }*/

        public Argument_Object(string[] args)
        {
            List<string> directs = new List<string>();
            List<string> fils = new List<string>();
            List<string> confs = new List<string>();
            List<string> output_playlists_list = new List<string>();
            List<string> input_playlists_list = new List<string>();
            List<string> input_playlist_directories_list = new List<string>();

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].StartsWith("-") || args[i].StartsWith("--"))
                {
                    if (args[i] == "-d" || args[i] == "--directory")
                        for (int j = i + 1; j < args.Length; j++)
                        {
                            if (args[j].StartsWith("-") || args[j].StartsWith("--"))
                                break;
                            else
                                directs.Add(args[j]);
                        }
                    if (args[i] == "-f" || args[i] == "--file")
                        for (int j = i + 1; j < args.Length; j++)
                        {
                            if (args[j].StartsWith("-") || args[j].StartsWith("--"))
                                break;
                            else
                                fils.Add(args[j]);
                        }
                    if (args[i] == "-p" || args[i] == "--playlists-output")
                        for (int j = i + 1; j < args.Length; j++)
                        {
                            if (args[j].StartsWith("-") || args[j].StartsWith("--"))
                                break;
                            else
                                output_playlists_list.Add(args[j]);
                        }
                    if (args[i] == "-y" || args[i] == "--playlists-input")
                        for (int j = i + 1; j < args.Length; j++)
                        {
                            if (args[j].StartsWith("-") || args[j].StartsWith("--"))
                                break;
                            else
                            {
                                if (System.IO.File.Exists(args[j])) input_playlists_list.Add(args[j]);
                                else if (System.IO.Directory.Exists(args[j]))
                                {
                                    //input_playlist_directories_list.Add(args[j]);
                                    input_playlists_list.AddRange(System.IO.Directory.GetFiles(args[j]).ToList());
                                }
                            }
                        }
                    if (args[i] == "-c" || args[i] == "--config-files")
                        for (int j = i + 1; j < args.Length; j++)
                        {
                            if (args[j].StartsWith("-") || args[j].StartsWith("--"))
                                break;
                            else
                                confs.Add(args[j]);
                        }
                }

            }
            //_input_playlist_directories = input_playlist_directories_list.ToArray();
            _config_files = confs.ToArray();
            _directories = directs.ToArray();
            _files = fils.ToArray();
            _output_playlists = output_playlists_list.ToArray();
            _input_playlists = input_playlists_list.ToArray();
            if (files.Length > 0)
                _firstFile = _files[0];
        }

        /// <summary>
        /// Returns fals if either there are no location directories or there are no destination files
        /// </summary>
        /// <returns></returns>
        public bool CheckOK()
        {
            bool x = false, y = false;
            if (_directories.Length != 0 || _input_playlists.Length != 0) x = true;
            if (_files.Length != 0 || _output_playlists.Length != 0) y = true;
            return (x && y) ? true : false;
        }
    }
}
