﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TagLib;

namespace App_shell
{
    interface ISong_Obj
    {
        /// <summary>
        /// Names all tags of a given song in the Console.
        /// </summary>
        void Name_All_Tags();

        /// <summary>
        /// Names all tags of a given song in a specific file.
        /// </summary>
        /// <param name="path"></param>
        void Name_All_Tags_FileWrite(string path);

        /// <summary>
        /// Retruns the tags of the song in a string fashion.
        /// </summary>
        /// <returns></returns>
        string Name_All_Tags_LineReturn();

        /// <summary>
        /// Creates a CSV file with all the tags.
        /// </summary>
        /// <param name="path"></param>
        string CSVString();
    }

    class Song_Obj : ISong_Obj
    {
        private string _songPath;
        private string _fileName = null;
        private TagLib.File _givenSong;

        public Song_Obj(string songPath)
        {
            _songPath = songPath;

            //Checks if the file exists and them createws the new TagLib file
            if (System.IO.File.Exists(songPath))
            {
                TagLib.File givenSong = TagLib.File.Create(_songPath);
                _givenSong = givenSong;
            }

            //Checks if file exists and checks how the path is writte
            //Than it takes the file name out of the filepath
            if (songPath.Contains("\\") && System.IO.File.Exists(songPath))
            {
                _fileName = _songPath.Split('\\').ToList().Last();
            }
            if(songPath.Contains("/") && System.IO.File.Exists(songPath))
            {
                _fileName = _songPath.Split('/').ToList().Last();
            }
            if (!songPath.Contains("/") && !songPath.Contains("\\") && System.IO.File.Exists(songPath))
                _fileName = songPath.Remove(songPath.Length - 4);
        }                               
        public string[] Artist
        {
            get
            {
                return _givenSong.Tag.Performers;
            }
        }
        public string Album
        {
            get
            {
                return _givenSong.Tag.Album;
            }
        }
        public string Name
        {
            get
            {
                return _givenSong.Tag.Title;
            }
        }
        public string Year
        {
            get
            {
                return _givenSong.Tag.Year.ToString();
            }
        }
        public string Track
        {
            get
            {
                return _givenSong.Tag.Track.ToString();
            }
        }
        public string Disc
        {
            get
            {
                return _givenSong.Tag.Disc.ToString();
            }
        }
        public string[] Genres
        {
            get
            {
                return _givenSong.Tag.Genres;
            }
        }
        public TimeSpan Duration
        {
            get
            {
                return _givenSong.Properties.Duration;
            }
        }
        public int DurationInSeconds
        {
            get
            {
                return (int)_givenSong.Properties.Duration.TotalSeconds;
            }
        }
        public string FileName
        {
            get
            {
                return _fileName;
            }
        }
        
        /// <summary>
        /// Returns the path of the current song
        /// </summary>
        public string SongPath
        {
            get
            {
                return _songPath;
            }
        }

        /// <summary>
        /// Names all tags of a given song in the Console.
        /// </summary>
        public void Name_All_Tags()
        {
            Console.WriteLine("");
            Console.WriteLine("============ SONG INFO ============");
            string[] Artisian = Artist;
            string[] Genres_temp = Genres;
            
                Console.Write("Artists: ");
                for (int iii = 0; iii < Artisian.Length; iii++)
                {
                    if (Artist.Length == 1)
                    {
                        Console.WriteLine(Artisian[0]);
                        break;
                    }
                    else if (iii == Artisian.Length - 2)
                    {
                        Console.WriteLine(Artisian[iii] + " & " + Artisian[iii + 1]);
                        break;
                    }
                    Console.Write(Artisian[iii] + ", ");
                }
            if(Name != null)
            {
                Console.WriteLine("Name: " + Name);
            }
            if(Album != null)
            {
                Console.WriteLine("Album: " + Album);
            }
            if (Year != null)
            {
                Console.WriteLine("Year: " + Year);
            }
            if(Track != null)
            {
                Console.WriteLine("Track: " + Track);
            }
            if(Disc != null)
            {
                Console.WriteLine("Disc: " + Disc);
            }
            
            if (Genres.Length != 0)
             {
                 Console.Write("Genres: ");
                 for (int ii = 0; ii < Genres_temp.Length; ii++)
                 {
                     if (Genres_temp.Length == 1)
                     {
                         Console.WriteLine(Genres_temp[0]);
                         break;
                     }
                     else if (ii == Genres_temp.Length - 2)
                     {
                         Console.WriteLine(Genres_temp[ii] + " & " + Genres_temp[ii + 1]);
                         break;
                     }
                     Console.Write(Genres_temp[ii] + ", ");
                 }
             }
             
            Console.WriteLine("===================================");
        }

        /// <summary>
        /// Names all tags of a given song in a specific file.
        /// </summary>
        /// <param name="path"></param>
        public void Name_All_Tags_FileWrite(string path)
        {
            string lineReturn = null;

            lineReturn += "\r\n";
            lineReturn += "============ SONG INFO ============\r\n";
            string[] Artisian = Artist;
            string[] Genres_temp = Genres;

            lineReturn += "Artists: ";
            for (int iii = 0; iii < Artisian.Length; iii++)
            {
                if (Artist.Length == 1)
                {
                    lineReturn += Artisian[0]+"\r\n";
                    break;
                }
                else if (iii == Artisian.Length - 2)
                {
                    lineReturn += Artisian[iii] + " & " + Artisian[iii + 1]+"\r\n";
                    break;
                }
                lineReturn += Artisian[iii] + ", ";
            }
            if (Name != null)
            {
                lineReturn += "Name: " + Name+"\r\n";
            }
            if (Album != null)
            {
                lineReturn += "Album: " + Album+"\r\n";
            }
            if (Year != null)
            {
                lineReturn += "Year: " + Year+"\r\n";
            }
            if (Track != null)
            {
                lineReturn += "Track: " + Track+"\r\n";
            }
            if (Disc != null)
            {
                lineReturn += "Disc: " + Disc+"\r\n";
            }

            if (Genres.Length != 0)
            {
                lineReturn += "Genres: ";
                for (int ii = 0; ii < Genres_temp.Length; ii++)
                {
                    if (Genres_temp.Length == 1)
                    {
                        lineReturn += Genres_temp[0]+"\r\n";
                        break;
                    }
                    else if (ii == Genres_temp.Length - 2)
                    {
                        lineReturn += Genres_temp[ii] + " & " + Genres_temp[ii + 1]+"\r\n";
                        break;
                    }
                    lineReturn += Genres_temp[ii] + ", ";
                }
            }

            lineReturn += "===================================\r\n";
            System.IO.File.WriteAllText(path, lineReturn);
        }

        /// <summary>
        /// Retruns the tags of the song in a string fashion.
        /// </summary>
        /// <returns></returns>
        public string Name_All_Tags_LineReturn()
        {
            string lineReturn = null;

            lineReturn += "\r\n";
            lineReturn += "============ SONG INFO ============\r\n";
            string[] Artisian = Artist;
            string[] Genres_temp = Genres;

            lineReturn += "Artists: ";
            for (int iii = 0; iii < Artisian.Length; iii++)
            {
                if (Artist.Length == 1)
                {
                    lineReturn += Artisian[0] + "\r\n";
                    break;
                }
                else if (iii == Artisian.Length - 2)
                {
                    lineReturn += Artisian[iii] + " & " + Artisian[iii + 1] + "\r\n";
                    break;
                }
                lineReturn += Artisian[iii] + ", ";
            }
            if (Name != null)
            {
                lineReturn += "Name: " + Name + "\r\n";
            }
            if (Album != null)
            {
                lineReturn += "Album: " + Album + "\r\n";
            }
            if (Year != null)
            {
                lineReturn += "Year: " + Year + "\r\n";
            }
            if (Track != null)
            {
                lineReturn += "Track: " + Track + "\r\n";
            }
            if (Disc != null)
            {
                lineReturn += "Disc: " + Disc + "\r\n";
            }

            if (Genres.Length != 0)
            {
                lineReturn += "Genres: ";
                for (int ii = 0; ii < Genres_temp.Length; ii++)
                {
                    if (Genres_temp.Length == 1)
                    {
                        lineReturn += Genres_temp[0] + "\r\n";
                        break;
                    }
                    else if (ii == Genres_temp.Length - 2)
                    {
                        lineReturn += Genres_temp[ii] + " & " + Genres_temp[ii + 1] + "\r\n";
                        break;
                    }
                    lineReturn += Genres_temp[ii] + ", ";
                }
            }

            lineReturn += "===================================\r\n";
            return lineReturn;
        }

        /// <summary>
        /// Retruns a string wiht all tags in CSV format sepparated by a ";" character.
        /// </summary>
        /// <returns></returns>
        public string CSVString()
        {
            string[] storm = Artist;
            string _storm = null;

            string[] theGenres = Genres;
            string _theGenres = null;

            for (int g = 0; g < storm.Length; g++)
            {
                if (storm.Length == 1)
                {
                    _storm += storm[0];
                    break;
                }
                else if (g == storm.Length - 2)
                {
                    _storm += storm[g] + " & " + storm[g + 1];
                    break;
                }
                _storm += storm[g] + ", ";
            }

            for (int g = 0; g < theGenres.Length; g++)
            {
                if (theGenres.Length == 1)
                {
                    _theGenres += theGenres[0];
                    break;
                }
                else if (g == theGenres.Length - 2)
                {
                    _theGenres += theGenres[g] + " & " + theGenres[g + 1];
                    break;
                }
                _theGenres += theGenres[g] + ", ";
            }

            return _storm + ";" + Name + ";" + Album + ";" + Year + ";" + _theGenres + ";" + Track + ";" + Disc;
        }

        /// <summary>
        /// Retruns a string wiht all tags in CSV format sepparated by the defined character.
        /// </summary>
        /// <param name="Character"></param>
        /// <returns></returns>
        public string CSVString(char Character)
        {
            string[] storm = Artist;
            string _storm = null;

            string[] theGenres = Genres;
            string _theGenres = null;

            for (int g = 0; g < storm.Length; g++)
            {
                if (storm.Length == 1)
                {
                    _storm += storm[0];
                    break;
                }
                else if (g == storm.Length - 2)
                {
                    _storm += storm[g] + " & " + storm[g + 1];
                    break;
                }
                _storm += storm[g] + ", ";
            }

            for (int g = 0; g < theGenres.Length; g++)
            {
                if (theGenres.Length == 1)
                {
                    _theGenres += theGenres[0];
                    break;
                }
                else if (g == theGenres.Length - 2)
                {
                    _theGenres += theGenres[g] + " & " + theGenres[g + 1];
                    break;
                }
                _theGenres += theGenres[g] + ", ";
            }

            return _storm + Character + Name + Character + Album + Character + Year + Character + _theGenres + Character + Track + Character + Disc;
        }
    }
}