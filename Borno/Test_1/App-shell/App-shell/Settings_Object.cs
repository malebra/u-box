﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_shell
{
    interface ISettings_Object
    {

    }

    class Settings_Object : ISettings_Object
    {
        public bool setup { get; private set; }
        public bool csv_sellect { get; private set; }
        public bool verbose { get; private set; }
        public bool config { get; private set; }
        public bool write { get; private set; }
        public bool config_file { get; private set; }
        public bool preserve { get; private set; }
        public bool recursive { get; private set; }
        public bool fix { get; private set; }
        public bool multiple_files { get; private set; }
        public bool multiple_directories { get; private set; }
        public bool random { get; private set; }
        public bool test { get; private set; }
        public bool nauticus { get; private set; }
        public bool input_playlists { get; private set; }
        public bool output_playlists { get; private set; }
        public bool file_exist { get; private set; }

        /*
        public bool setup;
        public bool csv_sellect;
        public bool verbose;
        public bool config;
        public bool write;
        public bool config_file;
        public bool preserve;
        public bool recursive;
        public bool fix;
        public bool multiple_files;
        public bool multiple_directories;
        public bool random;
        public bool test;
        public bool nauticus;
        */

        public Settings_Object(string[] args)
        {
            setup = false;
            csv_sellect = false;
            verbose = false;
            config = false;
            write = false;
            config_file = false;
            preserve = false;
            recursive = false;
            fix = false;
            multiple_files = false;
            multiple_directories = false;
            random = false;
            test = false;
            nauticus = false;
            input_playlists = false;
            output_playlists = false;
            file_exist = false;

            foreach (string h in args)
            {

                if (h == "setup")
                    setup = true;
                if (h == "config")
                    config = true;
                if (h == "test")
                    test = true;

                if (h.StartsWith("-") && !h.StartsWith("--"))
                {
                    if (h.Contains("v"))
                        verbose = true;
                    if (h.Contains("x"))
                        csv_sellect = true;
                    if (h.Contains("z"))
                    {
                        preserve = true;
                        config_file = true;
                    }
                    if (h.Contains("f"))
                    {
                        file_exist = true;
                        write = true;
                    }
                    if (h.Contains("p"))
                        preserve = true;
                    if (h.Contains("r"))
                        recursive = true;
                    if (h.Contains("k"))
                        write = false;
                    if (h.Contains("u"))
                        fix = true;
                    if (h.Contains("n"))
                        nauticus = true;
                    if (h.Contains("y"))
                        input_playlists = true;
                    if (h.Contains("p"))
                        output_playlists = true;
                }
                if (h.StartsWith("--"))
                {
                    if (h == "--verbose")
                        verbose = true;
                    if (h == "--preserve")
                        preserve = true;
                    if (h == "--recursive")
                        recursive = true;
                    if (h == "--no-write")
                        write = false;
                    if (h == "--fix")
                        fix = true;
                    if (h == "--random")
                        random = true;
                    if (h == "--playlists-input")
                        input_playlists = true;
                    if (h == "--playlists-output")
                        output_playlists = true;
                }
            }
        }

        public void CheckMultiples(Argument_Object args)
        {
            if (args.directories.Length > 1)
                multiple_directories = true;
            if (args.files.Length > 1)
                multiple_files = true;
        }
    }
}
