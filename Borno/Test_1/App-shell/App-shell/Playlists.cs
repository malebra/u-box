﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace App_shell
{
    class Playlist
    {
        static List<Playlist> _allPlaylists = new List<Playlist>(); //Contains the list of all playlists

        private Argument_Object _args = null; //Contains the command line arguments
        private string[] _playlists; //?????????????
        private string _playlist_path = null; //Contains the path of the playlist
        private List<Song_Obj> _song_list = new List<Song_Obj>(); //Constains the songs in the playlist


        /// <summary>
        /// Returns the path of all songs in the palylist
        /// </summary>
        public List<Song_Obj> SongList
        {
            get
            {
                return _song_list;
            }
        }
        /// <summary>
        /// Returns the path of the list
        /// </summary>
        public string GetPath
        {
            get
            {
                return _playlist_path;
            }
        }

        Playlist(Argument_Object arguments)
        {
            _args = arguments;
            _playlists = _args.output_playlists;
            _allPlaylists.Add(this);
        }
        Playlist(string path)
        {
            _playlist_path = path;
            if (System.IO.File.Exists(path))
                Extract();
            _allPlaylists.Add(this);
        }
        Playlist()
        {
            _playlist_path = System.IO.Path.GetTempFileName();
            //_playlist_path = null;
            _allPlaylists.Add(this);
        }
        
        /// <summary>
        /// Creates a playlist using an 'Argument_Object' as an argument
        /// </summary>
        /// <param name="argus"></param>
        /// <returns></returns>
        public static Playlist Create(Argument_Object argus)
        {
            Playlist play_play = new Playlist(argus);
            return play_play;

        }
        /// <summary>
        /// Creates a playlist using an 'path' as an argument
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Playlist Create(string path)
        {
            Playlist play_play = new Playlist(path);
            return play_play;
        }
        public static List<Playlist> CreateMany(string[] paths)
        {
            List<Playlist> temp = new List<Playlist>();
            foreach (string path in paths)
                temp.Add(Playlist.Create(path));
            return temp;
        }
        /// <summary>
        /// Creates an empty playlist without a path
        /// </summary>
        /// <returns></returns>
        public static Playlist Create()
        {
            Playlist play_play = new Playlist();
            return play_play;
        }
        /// <summary>
        /// Returns the list of all playlists
        /// </summary>
        /// <returns></returns>
        public static List<Playlist> AllPlaylists()
        {
            return _allPlaylists;
        }
        /// <summary>
        /// Exports the playlist in WMP Playlist format to the predefined path
        /// </summary>
        public void ExportPlayLists_WMP()
        {
            XmlDocument xmlPlaylist = new XmlDocument();
            XmlNode version = xmlPlaylist.CreateProcessingInstruction("wpl", "version=\"1.0\"");
            xmlPlaylist.AppendChild(version);
            XmlNode smil = xmlPlaylist.CreateElement("smil");
            xmlPlaylist.AppendChild(smil);

            XmlNode head = xmlPlaylist.CreateElement("head");
            smil.AppendChild(head);

            XmlNode meta = xmlPlaylist.CreateElement("meta");
            XmlAttribute attribute = xmlPlaylist.CreateAttribute("name");
            attribute.Value = "Generator";
            meta.Attributes.Append(attribute);
            attribute = xmlPlaylist.CreateAttribute("content");
            attribute.Value = "Microsoft Windows Media Player -- 12.0.10586.545";
            meta.Attributes.Append(attribute);

            head.AppendChild(meta);

            meta = xmlPlaylist.CreateElement("meta");
            attribute = xmlPlaylist.CreateAttribute("name");
            attribute.Value = "ItemCount";
            meta.Attributes.Append(attribute);
            attribute = xmlPlaylist.CreateAttribute("content");
            attribute.Value = _song_list.Count().ToString();
            meta.Attributes.Append(attribute);

            head.AppendChild(meta);

            XmlNode body = xmlPlaylist.CreateElement("body");
            smil.AppendChild(body);

            XmlNode seq = xmlPlaylist.CreateElement("seq");
            body.AppendChild(seq);

            foreach (Song_Obj song in _song_list)
            {
                XmlNode media = xmlPlaylist.CreateElement("media");
                attribute = xmlPlaylist.CreateAttribute("src");
                attribute.Value = song.SongPath;
                media.Attributes.Append(attribute);
                seq.AppendChild(media);
            }

            if (_playlist_path == null && _args != null)
                foreach (string playlist in _args.output_playlists)
                    xmlPlaylist.Save(playlist);
            else if (_playlist_path == null && _args == null) throw new NullReferenceException("There is no path defined for the playlist");
            else xmlPlaylist.Save(_playlist_path);
        }

        /// <summary>
        /// Exports the playlist in WMP Playlist format to 'path'
        /// </summary>
        public void ExportPlayLists_WMP(string path)
        {
            XmlDocument xmlPlaylist = new XmlDocument();
            XmlNode version = xmlPlaylist.CreateProcessingInstruction("wpl", "version=\"1.0\"");
            xmlPlaylist.AppendChild(version);
            XmlNode smil = xmlPlaylist.CreateElement("smil");
            xmlPlaylist.AppendChild(smil);

            XmlNode head = xmlPlaylist.CreateElement("head");
            smil.AppendChild(head);

            XmlNode meta = xmlPlaylist.CreateElement("meta");
            XmlAttribute attribute = xmlPlaylist.CreateAttribute("name");
            attribute.Value = "Generator";
            meta.Attributes.Append(attribute);
            attribute = xmlPlaylist.CreateAttribute("content");
            attribute.Value = "Microsoft Windows Media Player -- 12.0.10586.545";
            meta.Attributes.Append(attribute);

            head.AppendChild(meta);

            meta = xmlPlaylist.CreateElement("meta");
            attribute = xmlPlaylist.CreateAttribute("name");
            attribute.Value = "ItemCount";
            meta.Attributes.Append(attribute);
            attribute = xmlPlaylist.CreateAttribute("content");
            attribute.Value = _song_list.Count().ToString();
            meta.Attributes.Append(attribute);

            head.AppendChild(meta);

            XmlNode body = xmlPlaylist.CreateElement("body");
            smil.AppendChild(body);

            XmlNode seq = xmlPlaylist.CreateElement("seq");
            body.AppendChild(seq);

            foreach (Song_Obj song in _song_list)
            {
                XmlNode media = xmlPlaylist.CreateElement("media");
                attribute = xmlPlaylist.CreateAttribute("src");
                attribute.Value = song.SongPath;
                media.Attributes.Append(attribute);
                seq.AppendChild(media);
            }

            if (path == null && _args != null)
                foreach (string playlist in _args.output_playlists)
                    xmlPlaylist.Save(playlist);
            else if (path == null && _args == null) throw new NullReferenceException("There is no path defined for the playlist");
            else xmlPlaylist.Save(path);
        }
        /// <summary>
        /// Changes the path of the playlist
        /// </summary>
        /// <param name="path"></param>
        public void ChangePath(string path)
        {
            _playlist_path = path;
        }

        public void Extract()
        {
            string path = _playlist_path;
            if (path != null)
            {
                XmlReader reader = XmlReader.Create(path);
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "media") _song_list.Add(new Song_Obj(reader.GetAttribute("src")));
                    }
                }
            }
        }

        void AddList(Playlist theList)
        {
            _allPlaylists.Add(theList);
        }

        public void Add(string path)
        {
            _song_list.Add(new Song_Obj(path));
        }
        public void Add(string[] paths)
        {
            foreach (string path in paths) _song_list.Add(new Song_Obj(path));
        }
        public void Add(Song_Obj song)
        {
            _song_list.Add(song);
        }
        public void Add(List<Song_Obj> songs)
        {
            _song_list.AddRange(songs);
        }
        public void Add(Song_Obj[] songs)
        {
            _song_list.AddRange(songs.ToList<Song_Obj>());
        }
    }
}

/*
 class Playlists
    {
        static List<Playlists> _allPlaylists = new List<Playlists>(); //Contains the list of all playlists

        private Argument_Object _args = null; //Contains the command line arguments
        private string[] _playlists; //?????????????
        private string _path = null; //Contains the path of the playlist
        private List<string> _song_list = new List<string>(); //Constains the songs in the playlist

        /// <summary>
        /// Returns the path of all songs in the palylist
        /// </summary>
        public List<string> SongList
        {
            get
            {
                return _song_list;
            }
        }
        /// <summary>
        /// Returns the path of the list
        /// </summary>
        public string GetPath
        {
            get
            {
                return _path;
            }
        }

        Playlists(Argument_Object arguments)
        {
            _args = arguments;
            _playlists = _args.output_playlists;
            _allPlaylists.Add(this);
        }
        Playlists(string path)
        {
            _path = path;
            if (System.IO.File.Exists(path))
                Extract();
            _allPlaylists.Add(this);
        }
        Playlists()
        {
            _path = null;
            _allPlaylists.Add(this);
        }

        /// <summary>
        /// Creates a playlist using an 'Argument_Object' as an argument
        /// </summary>
        /// <param name="argus"></param>
        /// <returns></returns>
        public static Playlists Create(Argument_Object argus)
        {
            Playlists play_play = new Playlists(argus);
            return play_play;

        }
        /// <summary>
        /// Creates a playlist using an 'path' as an argument
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Playlists Create(string path)
        {
            Playlists play_play = new Playlists(path);
            return play_play;
        }
        /// <summary>
        /// Creates an empty playlist without a path
        /// </summary>
        /// <returns></returns>
        public static Playlists Create()
        {
            Playlists play_play = new Playlists();
            return play_play;
        }

        public void ExportPlayLists_WMP()
        {
            XmlDocument xmlPlaylist = new XmlDocument();
            XmlNode version = xmlPlaylist.CreateProcessingInstruction("wpl", "version=\"1.0\"");
            xmlPlaylist.AppendChild(version);
            XmlNode smil = xmlPlaylist.CreateElement("smil");
            xmlPlaylist.AppendChild(smil);

            XmlNode head = xmlPlaylist.CreateElement("head");
            smil.AppendChild(head);

            XmlNode meta = xmlPlaylist.CreateElement("meta");
            XmlAttribute attribute = xmlPlaylist.CreateAttribute("name");
            attribute.Value = "Generator";
            meta.Attributes.Append(attribute);
            attribute = xmlPlaylist.CreateAttribute("content");
            attribute.Value = "Microsoft Windows Media Player -- 12.0.10586.545";
            meta.Attributes.Append(attribute);

            head.AppendChild(meta);

            meta = xmlPlaylist.CreateElement("meta");
            attribute = xmlPlaylist.CreateAttribute("name");
            attribute.Value = "ItemCount";
            meta.Attributes.Append(attribute);
            attribute = xmlPlaylist.CreateAttribute("content");
            attribute.Value = _song_list.Count().ToString();
            meta.Attributes.Append(attribute);

            head.AppendChild(meta);

            XmlNode body = xmlPlaylist.CreateElement("body");
            smil.AppendChild(body);

            XmlNode seq = xmlPlaylist.CreateElement("seq");
            body.AppendChild(seq);

            foreach (string song in _song_list)
            {
                XmlNode media = xmlPlaylist.CreateElement("media");
                attribute = xmlPlaylist.CreateAttribute("src");
                attribute.Value = song;
                media.Attributes.Append(attribute);
                seq.AppendChild(media);
            }

            if (_path == null && _args != null)
                foreach (string playlist in _args.output_playlists)
                    xmlPlaylist.Save(playlist);
            else if (_path == null && _args == null) throw new NullReferenceException("There is no path defined for the playlist");
            else xmlPlaylist.Save(_path);
        }
        
        public static List<Playlists> AllPlaylists()
        {
            return _allPlaylists;
        }

        public void Extract()
        {
            string path = _path;
            if (path != null)
            {
                XmlReader reader = XmlReader.Create(path);
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "media") _song_list.Add(reader.GetAttribute("src"));
                    }
                }
            }
        }

        void AddList(Playlists theList)
        {
            _allPlaylists.Add(theList);
        }

        public void AddSong(string path)
        {
            _song_list.Add(path);
        }

    }
 */
