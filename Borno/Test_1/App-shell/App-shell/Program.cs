﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMPLib;
using System.Media;
using TagLib;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace App_shell
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1 && (args[0] == "--help" || args[0] == "help"))
            {
                Console.WriteLine(System.IO.File.ReadAllText(@"C:\Users\Borno\Documents\help.txt"));
                return;
            }

            Argument_Object myArguments = new Argument_Object(args);
            Settings_Object configured = new Settings_Object(args);
            Playlist xxx = Playlist.Create(myArguments);
            List<string> xyz = new List<string>();

            //Check if directory and file location is present in arguments
            if (!myArguments.CheckOK() && !configured.test)
            {
                Console.WriteLine("[WARNING:]   You've probably not entered your directories or files location!");
                return;
            }

            

            //checks if there are multiple output files..... outputs same file to all of them
            configured.CheckMultiples(myArguments);

            string theList = null;
            string csv_file_data = null;
            string nauticus_file = null;
            string commonListFile = null;

            //this shit over here saves the file in the "theList" var
            if (configured.file_exist)
                if (configured.preserve && System.IO.File.Exists(myArguments.files[0]))
                    foreach (string line in System.IO.File.ReadAllLines(myArguments.files[0]))
                        theList += line + "\r\n";


            //recursive files & non-recursive files
            //bbb contains the location of every song
            string[] bbb = Locations(myArguments, configured); //old one goes : string[] bbb = hope.files;

            //Expporting file location
            if (configured.file_exist) commonListFile = myArguments.files[0];      //Old peset after "=" was: hope.conf_file;

            if (commonListFile == null && configured.file_exist)
            {
                Console.WriteLine("FUCK  FUCK  FUCK  FUCK  FUCK  FUCK  FUCK  FUCK  FUCK  FUCK  ");
                return;
            }


            ////////////////////////////////////////////////////////////////////////////////    TEST ARENA    /////////////////////////////////////////////////////////////
            //HERE YOU CAN TEST THE PROGRAM

            if (configured.test)
            {
                Algorythm.Crude(myArguments, configured);
                return;
            }
            
            ////////////////////////////////////////////////////////////////////////////////    TEST ARENA    /////////////////////////////////////////////////////////////
             
            foreach (string h in bbb)
            {
                if (AcceptableFile(h))
                {
                    Song_Obj u = new Song_Obj(h);
                    if (u.FileName != null)
                    {
                        if (configured.verbose)
                            Console.WriteLine(h);
                        if (configured.write && !configured.csv_sellect)
                        {
                            theList += u.Name_All_Tags_LineReturn();
                            theList += "\n";
                        }
                        if (configured.csv_sellect)
                        {
                            csv_file_data += u.CSVString();
                            csv_file_data += "\r\n";
                        }
                        else if (configured.nauticus)
                        {
                            nauticus_file += u.FileName + "\r\n";
                        }
                        if (configured.output_playlists) xyz.Add(h);
                    }
                }
                //////////
                xxx.ExportPlayLists_WMP();
            }

            if (configured.write && !configured.csv_sellect) System.IO.File.WriteAllText(commonListFile, theList);
            if (configured.csv_sellect && configured.write) System.IO.File.WriteAllText(commonListFile, csv_file_data);
            if (configured.nauticus && configured.write) System.IO.File.WriteAllText(commonListFile, nauticus_file, Encoding.UTF8);
            
            Console.WriteLine("\r\n******************** >>>> Task Completed <<<< ********************\r\n");

            for (int i = 1; i < myArguments.files.Length; i++)
                if (!System.IO.File.Exists(myArguments.files[i]))
                    System.IO.File.Copy(myArguments.files[0], myArguments.files[i]);

            Console.ForegroundColor = ConsoleColor.Blue;
            foreach (string name in myArguments.files)
                Console.WriteLine("Created file: " + name);
            foreach (string playlist in myArguments.output_playlists)
                Console.WriteLine("Created playlist: " + playlist);
            Console.ForegroundColor = ConsoleColor.White;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Returns TRUE if file is acceptable for taking out tags
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        static bool AcceptableFile(string h)
        {
            string[] allFileFormats = { ".mp3", ".flac" };

            if (System.IO.Path.GetExtension(h) == ".lnk")
                return false;

            foreach (string fileFormat in allFileFormats)
                if (System.IO.Path.GetExtension(h) == fileFormat)
                    return true;
            return false;
        }
        
        /// <summary>
        /// Finds and returns the location of every file inside the directory ("possibly recursive")
        /// </summary>
        /// <param name="args"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        static string[] Locations(Argument_Object args, Settings_Object config)
        {
            string[] locations = null;
            //string[] nu_directories = null;                             //Here we save the directories inside of the directories
            
            List<string> files = new List<string>();                    //List containing all the files's locations 
            
            foreach (string directory in RemoveSubdirectories(args.directories))
            {
                if (System.IO.Directory.Exists(directory))
                {
                    files.AddRange(System.IO.Directory.GetFiles(directory).ToList().Where(x => AcceptableFile(x)));
                    if (config.recursive)
                        files.AddRange(Recursion(directory, config).ToList());
                }
            }
            locations = files.ToArray();
            if (locations.Length == 0)
                Console.WriteLine("[ERROR:]    It appears non of the above given directories have files in them:");

            return locations;
        }

        /// <summary>
        /// Selfexplanitory
        /// </summary>
        /// <param name="directories"></param>
        /// <returns></returns>
        static string[] RemoveSubdirectories(string[] directories)
        {
            List<string> temp_list = new List<string>();

            bool test;

            for (int i = 0; i < directories.Length; i++)
            {
                test = true;
                for (int j = 0; j < directories.Length; j++)
                {
                    if (j != i)
                    {
                        if (directories[i].StartsWith(directories[j]))
                        {
                            test = false;
                            break;
                        }
                    }
                }
                if (test) temp_list.Add(directories[i]);
            }
            return temp_list.ToArray();
        }
        
        /// <summary>
        /// Recursivly terurns a string[] of every file in every subfolder of the map
        /// </summary>
        /// <param name="path"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        static string[] Recursion(string path, Settings_Object config)
        {
            string[] directories = null;
            string[] newFiles = null;
            List<string> file_list = new List<string>();

            directories = System.IO.Directory.GetDirectories(path);

            if (directories != null)
            {
                foreach (string dir in directories)
                {
                    if (System.IO.Directory.Exists(dir))
                    {
                        try
                        {
                            if (config.verbose)
                                Console.WriteLine("Entered the directory: " + dir + "\r\n========== FILES ==========");
                            string[] files = System.IO.Directory.GetFiles(dir).ToList().Where(m => AcceptableFile(m)).ToArray();

                            file_list.AddRange(files.ToList());

                            if (config.verbose) file_list.ForEach(x => Console.WriteLine("   =====> " + x));
                        }
                        catch (UnauthorizedAccessException) { }
                    }
                    try
                    {
                        newFiles = Recursion(dir, config);
                        file_list.AddRange(newFiles.ToList());
                        if (config.verbose) newFiles.ToList().ForEach(x => Console.WriteLine("   =====> " + x));
                    }
                    catch (UnauthorizedAccessException) { }
                    catch (DirectoryNotFoundException) { }
                }
            }
            return file_list.ToArray() ;
        }
    }
}